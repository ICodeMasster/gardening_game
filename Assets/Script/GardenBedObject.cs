using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GardenBedObject : MonoBehaviour
{
    public GameObject plantObject;
    private Plant plant;
    // Start is called before the first frame update
    void Start()
    {
        plant = plantObject.GetComponent<Plant>();
        Instantiate(plantObject, transform.position + plant.spawnPosition, plantObject.transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
