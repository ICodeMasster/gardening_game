using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class Plant : MonoBehaviour
{
    // Start is called before the first frame update

    public float growRate;
    public float maxSize = 3;
    public float newChildOffset = 1.0f;
    public Vector3 spawnPosition;
    private TimeHandler timeHandler;
    private bool isGrowing = true;
    public static float width  = 1;
    public static float height = 1;
    private List<MeshFilter> filter;
    private MeshRenderer renderer;
    void Start()
    {
        filter.Add(GetComponent<MeshFilter>());
        // Timer
        TimeHandler.OnTick += TimeHandler_OnTick;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void  TimeHandler_OnTick (object sender, TimeHandler.OnTickEventArgs e){
        if (isGrowing){
            filter.Add(GetComponent<MeshFilter>());
            MeshFilter[] filterArray = filter.ToArray();
            CombineInstance[] combine = new CombineInstance[filter.Count];
            int i = 0;
            while (i < filterArray.Length){
                combine[i].mesh = filterArray[i].sharedMesh;
                combine[i].transform = filterArray[i].transform.localToWorldMatrix;
            }
            if (transform.localScale.y >= maxSize) {
                isGrowing = false;
                TimeHandler.OnTick -= TimeHandler_OnTick;
            }
        }
    }
    private void createNewPlantSection (){

    }
}
