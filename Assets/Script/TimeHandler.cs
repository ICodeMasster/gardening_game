using System;
using UnityEngine;

public class TimeHandler : MonoBehaviour
{
    public static event EventHandler<OnTickEventArgs> OnTick;
    public class OnTickEventArgs: EventArgs {
        public int tick;
    }
    private int tick;
    private float tickTimer;
    private float TICK_TIMER_MAX = 0.2f;
    public int tickRate = 200;
    // Start is called before the first frame update
    void Start()
    {
        TICK_TIMER_MAX = (float) tickRate/1000;
        Debug.Log(TICK_TIMER_MAX);
    }

    // Update is called once per frame
    void Update()
    {
        tickTimer += Time.deltaTime;
        if (tickTimer >= TICK_TIMER_MAX ){
            tickTimer -= TICK_TIMER_MAX;
            tick ++;
            if (OnTick != null) OnTick(this, new OnTickEventArgs {tick = tick});
        }
    }
}
