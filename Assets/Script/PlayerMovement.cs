using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float maxSpeed = 20;
    public float acceleration = 3;
    public float minSpeed = 5;
    public float minZoom;
    public float maxZoom;
    public float zoomSpeed = 0.3f;
    public float rotateSpeed = 10f;
    private Vector3 motion;
    private float speed;
    private Camera cam;
    public GameObject focalPoint;
    private Vector3 mouseLoc;
    // Start is called before the first frame update
    void Awake()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        moveFocus();
        cameraZoom();
        cameraRotate();
    }
    private void moveFocus (){
        float xInput = Input.GetAxis("Horizontal");
        float zInput = Input.GetAxis("Vertical");
        Vector3 dir = transform.forward * zInput + transform.right * xInput;
        if (dir != new Vector3(0,0,0) ){
            if (speed <= maxSpeed ) speed += acceleration * Time.deltaTime;
        }
        else {
            speed = minSpeed;
        }
        transform.position += dir * speed * Time.deltaTime;
    }
    private void cameraZoom (){
        float scrollInput = Input.GetAxis("Mouse ScrollWheel");
        float dist = Vector3.Distance(transform.position, cam.transform.position);
        if(dist < minZoom && scrollInput > 0.0f)
            return;
        else if(dist > maxZoom && scrollInput < 0.0f)
            return;

        cam.transform.position += cam.transform.forward * scrollInput * zoomSpeed;
    }
    private void cameraRotate () {
        if (Input.GetMouseButton(2)) {
            Vector3 mouseDelta = mouseLoc - Input.mousePosition;
            mouseDelta.x = mouseDelta.x/Screen.width;
            mouseDelta.y = mouseDelta.y/Screen.height;
            Debug.Log(mouseDelta);
            transform.Rotate(new Vector3(mouseDelta.y *rotateSpeed, mouseDelta.x * rotateSpeed, 0 ), Space.World);
            mouseLoc = Input.mousePosition;
        }
        else {
            mouseLoc = Input.mousePosition;
        }
    }
    public void FocusOnPosition (Vector3 position) {

    }
}
