using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class ClickHandler : MonoBehaviour
{
    public GameObject gardenBed;

    // Start is called before the first frame update
    void Start()
    {

    }
     public float distance = 50f;
    // Update is called once per frame
    void Update()
    {
        if( Input.GetMouseButtonDown(0)){

             try {
                 Instantiate(gardenBed, getMouseClickPosition(), Quaternion.identity);
             }
             finally {

             }
        }
    }
    public Vector3 getMouseClickPosition () {
        //create a ray cast and set it to the mouses cursor position in game
        Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast (ray, out hit, distance))
        {
            Debug.Log("Clickhit" + hit.point);
            return hit.point;
        }
        else {
            throw new InvalidClickPositionException("Click Position Invalid");
        }
    }
}
[Serializable]
public class InvalidClickPositionException : Exception {
    public InvalidClickPositionException() : base() { }
    public InvalidClickPositionException(string message) : base(message) { }
    public InvalidClickPositionException(string message, Exception inner) : base(message, inner) { }

    // A constructor is needed for serialization when an
    // exception propagates from a remoting server to the client.
    protected InvalidClickPositionException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
}
